FROM debian:stable-slim
EXPOSE 6666
ENV ANOPE_VERSION="2.0.6" 
RUN apt-get update \
 && apt-get upgrade -y \
 && apt-get install -y --no-install-recommends \
    ca-certificates \
    wget \
    build-essential \
    curl \
    cmake \
    file \
    expect \
    libssl-dev \
    exim4
RUN groupadd -r ircd && useradd -r -m -g ircd ircd

COPY exim4 /etc/exim4
COPY deploy-anope.sh /home/ircd/deploy-anope.sh
COPY anope-make.expect /home/ircd/anope-make.expect
COPY Makefile /home/ircd/Makefile

USER ircd
WORKDIR /home/ircd
ENV HOME /home/ircd
RUN /home/ircd/deploy-anope.sh
RUN mkdir -p /home/ircd/logs

USER root
COPY entrypoint.sh .
ENTRYPOINT ./entrypoint.sh
